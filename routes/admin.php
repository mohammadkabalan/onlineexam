<?php


use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin','middleware' =>'auth:admin' ,'namespace'=>'Admin'],function(){

    Route::get('/home' ,'DashboardController@index')->name('admin.dashboard') ;
    Route::get('logout','AdminController@logout')->name('admin.logout') ;

     ///Student Route
    Route::get('std/show','StdController@showAllStudent')->name('show.all.std');
    Route::get('std/create','StdController@getCreatePage')->name('create.std');
    Route::post('std/save/','StdController@addStudent')->name('save.std');


    ///Subject Route
    Route::get('sub/show/all','SubjectController@showAllSubject')->name('show.all.sub');
    Route::get('sub/create','SubjectController@getCreateSubjectPage')->name('create.sub.page');
    Route::post('sub/save/','SubjectController@addSubject')->name('save.sub');

    ///Question Route
    Route::get('que/show/all','QuestionController@showAllQuestion')->name('show.all.que');
    Route::get('que/create','QuestionController@getCreateQuestionPage')->name('create.que.page');
    Route::post('que/save/','QuestionController@addQuestion')->name('save.que');

    ///Options Route
    Route::get('opt/show/all','OptionController@showAllOption')->name('show.all.opt');
    Route::get('opt/create','OptionController@getCreateOptionPage')->name('create.opt.page');
    Route::post('opt/save/','OptionController@addOption')->name('save.opt');

    ///Result Route
//    Route::get('result/show/all/Api/','ResultController@showAllResultAPI');
    Route::get('result/show/all','ResultController@showAllResultView')->name('show.all.res');
    Route::get('result/export/csv','ResultController@export')->name('export.res');

});


Route::group(['prefix' => 'Admin','middleware' => 'guest:admin' ,'namespace'=>'Admin'],function() {

    Route::get('/','LoginController@getlogin')->name('get.login.page') ;
    Route::post('login', 'LoginController@login')->name('admin.login');
});




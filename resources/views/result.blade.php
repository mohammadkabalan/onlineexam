@extends('layouts.app')

@section('content')
    <div id="wrapper">
        <div class="container">
            <div class="row text-center">
                <h1>Your Mark :
                        @if($result > 50)
                         <span class="badge badge-success">
                            {{ $result }}
                        </span></h1>
                        @else
                        <span class="badge badge-danger">
                            {{ $result }}
                        </span>
                        @endif
                    </h1>
                <div class="check_mark_img">
                    <img src="{{ asset('frontend/images/completed.png') }}" alt="image_not_found">
                </div>
                <div class="sub_title">
                    <span>Your submission has been received</span>
                </div>
                <div class="title pt-1">
                    <h3>Thank you For your Participation!</h3>
                </div>
            </div>
        </div>
    </div>
@endsection

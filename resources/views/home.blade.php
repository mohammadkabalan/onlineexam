@extends('layouts.app')

@section('content')

    <form action="{{ route('exam.result') }}" method="post">
        @csrf
            <div class="container1">
                <div class="row-fluid">
                    <!-- my php code which uses x-path to get results from xml query. -->
                @if(!empty($question))
                    @for($i = 0 ; $i < count($question) ; $i++)
                    <div class="col-sm-4 ">
                        <div class="card">
                            <div class="box">
                                <div class="img">
                                    <img src="{{ asset('frontend/images/png-clipart-question.png') }}">
                                </div>



                                <h2><span>Question {{ $i+1 }} </span><br> {{ $question[$i]->question_text }}</h2>
                                @for($j = 0 ; $j < count($options) ; $j++ )
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="{{ $options[$i][$j]->id }}" name="questions[{{ $question[$i]->id }}]" id="{{$i}}">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            {{ $options[$i][$j]->option_text  }}
                                        </label>
                                    </div>
                                @endfor

                            </div>
                        </div>
                    </div> <br>
                    @endfor
                    @else
                    @endif
                </div>
            </div> <!--container div  -->
        <div class="container1">
            <div class="row-fluid">
                <div class="form-group">
                    <div class="col-md-9 text-center">
                        <button type="submit" class="btn btn-primary" style="padding: 12px ; width:220px ; margin-left: 69px;">
                            Submit
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

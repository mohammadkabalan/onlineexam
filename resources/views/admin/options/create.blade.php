@extends('admin.layout.master')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <!--content here Start -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row d-flex justify-content-center">
                            <!-- left column -->
                            <div class="col-md-6">
                                <!-- general form elements -->
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Options Form</h3>
                                    </div>
                                    <div class="">

                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>

                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form action="{{ route('save.opt') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label>{{ __('Question') }}</label>
                                                <select class="form-control" name="que_id" >
                                                    @foreach($question as $key => $que)
                                                        <option value="{{ $que->id }}">{{ $que->question_text }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Option</label>
                                                <input type="text" class="form-control" name="opt_name" placeholder="Options">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Points</label>
                                                <input type="number" class="form-control" name="points" placeholder="0" min="0">
                                            </div>


                                        </div>
                                        <!-- /.card-body -->
                                        <div class="card-footer" >
                                            <button type="submit" style="width:100%" class="btn btn-success d-flex justify-content-center">Create</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /. end card -->



                            </div>
                            <!--/.col (left) -->

                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </section>
                <!--content here End -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->


    </div>
    <!-- /.row -->


    <script type="text/javascript">
        function readURL(input){
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#avatar')
                        .attr('src', e.target.result)
                        .width("56%")
                        .height("126px") ;
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection

@extends('admin.layout.master')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <!--content here Start -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row d-flex justify-content-center">
                            <!-- left column -->
                            <div class="col-md-6">
                                <!-- general form elements -->
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Students Form</h3>
                                    </div>
                                    <div class="">

                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>

                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form action="{{ route('save.std') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Name</label>
                                                <input type="text" class="form-control" name="std_name" placeholder="Mark">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email</label>
                                                <input type="email" class="form-control" name="std_email" placeholder="email@email.com">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Password</label>
                                                <input type="password" class="form-control" name="std_password" placeholder="######">
                                            </div>


                                        </div>
                                        <!-- /.card-body -->
                                        <div class="card-footer" >
                                            <button type="submit" style="width:100%" class="btn btn-success d-flex justify-content-center">Create</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /. end card -->



                            </div>
                            <!--/.col (left) -->

                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </section>
                <!--content here End -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->


    </div>
    <!-- /.row -->


    <script type="text/javascript">
        function readURL(input){
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#avatar')
                        .attr('src', e.target.result)
                        .width("56%")
                        .height("126px") ;
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection

@extends('layouts.app')

@section('content')

    <div id="wrapper" style="margin-top:143px;">

        <div class="container">

            <div class="row text-center">
                <h1>Your Mark :
                    @if($data[0]->total_points > 50)
                        <span class="badge badge-success">
                            {{ $data[0]->total_points }}
                        </span></h1>
                @else
                    <span class="badge badge-danger">
                            {{ $data[0]->total_points }}
                        </span>
                    @endif
                    </h1>
                <div class="check_mark_img">
                    <img src="{{ asset('frontend/images/passed.jpg') }}" alt="image_not_found">
                </div>
                <div class="sub_title">
                    <span>Your submission has been received</span>
                </div>
                <div class="title pt-1">
                    <h3>You can try just one time</h3>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('admin.layout.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <!--content here Start -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Responsive Hover Table</h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>VisaName</th>
                                <th>Price</th>
                                <th>Conditions</th>
                                <th>Image</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($visas as $key => $visa)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $visa->visa_name }}</td>
                                    <td><span class="badge badge-success"> {{ $visa->price }} </span></td>
                                    <td><span class="badge badge-success"> {{ $visa->conditions }}</span></td>
                                    <td><img src="{{ asset('storage/'.$visa->image) }}" style="width:100px ; height:100px;" alt=""></td>
                                    <td>{{ \Carbon\Carbon::parse($visa->created_at)->diffForhumans() }}</td>
                                    @if($visa->status == 1)
                                        <td>Active</td>
                                    @else
                                        <td>Pending</td>
                                    @endif
                                    <td>
                                        <a href="{{ route('edit.visa',$visa->id) }}" class="btn btn-sm btn-success" title="edit"><i class="fa fa-edit"></i></a>
                                        <a href="{{ route('delete.visa',$visa->id) }}" class="btn btn-sm btn-danger"title="delete" id="delete"><i class="fa fa-trash"></i></a>
                                        <a href="" class="btn btn-sm btn-warning"title="show"><i class="fa fa-eye"></i></a>
                                        @if($visa->status == 1)
                                            <a href="{{ route('inactive.visa',$visa->id) }}" class="btn btn-sm btn-danger" title="inactive"><i class="fa fa-thumbs-down"></i></a>
                                        @else
                                            <a href="{{ route('active.visa',$visa->id) }}" class="btn btn-sm btn-info" title="active"><i class="fa fa-thumbs-up"></i></a>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!--content here End -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->


    </div>
    <!-- /.row -->



@endsection




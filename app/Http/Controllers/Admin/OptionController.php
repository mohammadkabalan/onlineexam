<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Option;
use App\Models\Question;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin') ;
    }
    public function showAllOption()
    {
        $options = Option::all();

        return view('admin.options.index',compact('options')) ;
    }

    public function getCreateOptionPage()
    {
        $question = Question::all() ;
        return view('admin.options.create',compact('question')) ;
    }

    public function addOption(Request $request)
    {
        $result =  Option::create([
            'option_text'     => $request->opt_name,
            'points'          => $request->points ,
            'question_id'     => $request->que_id ,
        ]) ;

        if($result)
        {
            $notification = array(
                'message' => 'Created Successfully' ,
                'alert-type' => 'success'
            );
        }
        else
        {
            $notification = array(
                'message' => 'There is something error' ,
                'alert-type' => 'error'
            );
        }
        return redirect()->back()->with($notification) ;
    }
}

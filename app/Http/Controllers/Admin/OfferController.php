<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BookConcert;
use App\Models\Offer;
use Illuminate\Http\Request;

class OfferController extends Controller
{
   public function __construct()
   {
       $this->middleware('auth:admin') ;
   }

   public function get_offer_page()
   {
       $offers = Offer::all();
       return view('admin.offers.index',compact('offers')) ;
   }
   public function get_page_create_offer()
   {
      return view('admin.offers.create') ;
   }
   public function add_offer(Request $request)
   {

       $vliadatedata =  $request->validate([
                       'offer_name'  =>  'required|string|max:200' ,
                       'expiry_date' =>  'required|numeric' ,
                       'offer_logo'  =>  'required' ,
                   ]);


       $duration = new \DateTime('+'.$request->expiry_date.'Day') ;


       $offer_img = $request->offer_logo ;
       $folder_name = 'offers' ;
       $offer_img_upload = Upload_Image_Helper('public',$offer_img,$folder_name) ;

       $result =  Offer::create([
                      'offer_name' => $request->offer_name ,
                      'image' => $offer_img_upload ,
                      'expiry_date' => $duration->format('Y-m-d H:i:s'),
                      'status' => 0
                  ]) ;

       if($result)
       {
           $notification = array(
               'message' => 'Created Successfully' ,
               'alert-type' => 'success'
           );
       }
       else
       {
           $notification = array(
               'message' => 'There is something error' ,
               'alert-type' => 'error'
           );
       }

       return redirect()->back()->with($notification) ;
   }

    public function activeOffer($id)
    {
        Offer::where('id',$id)->update([
                             'status' => 1 ,
                         ]);
        $notification = array(
            'message' => 'Offer Active Successfully ' ,
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification) ;
    }

    public function inctiveOffer($id)
    {
        Offer::where('id',$id)->update([
                           'status' => 0 ,
                       ]);
        $notification = array(
            'message' => 'Offer Inactive Successfully ' ,
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification) ;
    }
    public function delete() {}
    public function edit() {}
    public function update() {}


   public function get_all_booking_concert()
   {
       $users = BookConcert::all() ;
       return view('admin.offers.concerts',compact('users'));
   }
}

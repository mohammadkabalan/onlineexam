<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Models\Subject;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin') ;
    }
    public function showAllQuestion()
    {
        $question = Question::all();

        return view('admin.question.index',compact('question')) ;
    }

    public function getCreateQuestionPage()
    {
        $subject = Subject::all() ;
        return view('admin.question.create',compact('subject')) ;
    }

    public function addQuestion(Request $request)
    {
        $result =  Question::create([
            'question_text'     => $request->que_name,
            'subject_id'    => $request->sub_id ,
        ]) ;

        if($result)
        {
            $notification = array(
                'message' => 'Created Successfully' ,
                'alert-type' => 'success'
            );
        }
        else
        {
            $notification = array(
                'message' => 'There is something error' ,
                'alert-type' => 'error'
            );
        }
        return redirect()->back()->with($notification) ;
    }
}

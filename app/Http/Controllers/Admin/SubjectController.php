<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function showAllSubject()
    {
        $subjects = Subject::all() ;
        return view('admin.subject.index',compact('subjects'));
    }
    public function getCreateSubjectPage()
    {
        return view('admin.subject.create') ;
    }
    public function addSubject(Request $request)
    {
        $result =  Subject::create([
            'name'     => $request->sub_name,
        ]) ;
        if($result)
        {
            $notification = array(
                'message' => 'Created Successfully' ,
                'alert-type' => 'success'
            );
        }
        else
        {
            $notification = array(
                'message' => 'There is something error' ,
                'alert-type' => 'error'
            );
        }
        return redirect()->back()->with($notification) ;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function getlogin()
    {
        if(Auth::id())
        {
            return redirect()->back();
        }
        else
        {
            return view('admin.auth.login') ;
        }
    }


    public function login(LoginRequest $request)
    {

        $remember_me = $request->has('remember_me') ? true : false ;

        // return  $request ;
        if(auth()->guard('admin')->attempt(['email' => $request->input("email") , 'password' => $request->input("password")]))
        {
            //here can add option notify() ;
            $userName = User::select('name')->get() ;

            return redirect()->route('admin.dashboard') ;
        }
        //here can add option notify() ;
        return  redirect()->back()->with(['error' => 'البيانات المدخله ليست صحيحه']) ;

    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ResultExport;
use App\Http\Controllers\Controller;
use App\Models\Result;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ResultController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function showAllResultView()
    {
        $result = Result::all() ;
        return view('admin.result.index',compact('result'));
    }
    public function export()
    {
        return Excel::download(new ResultExport(), 'result.xlsx');
    }

}

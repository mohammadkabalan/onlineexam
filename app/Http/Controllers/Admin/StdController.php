<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StdController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin') ;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showAllStudent()
    {
        $students = User::all();

        return view('admin.students.index',compact('students')) ;
    }

    public function getCreatePage()
    {
        return view('admin.students.create') ;
    }

    public function addStudent(Request $request)
    {
        $result =  User::create([
            'name'     => $request->std_name,
            'email'    => $request->std_email ,
            'password' => Hash::make($request->std_password),
        ]) ;

        if($result)
        {
            $notification = array(
                'message' => 'Created Successfully' ,
                'alert-type' => 'success'
            );
        }
        else
        {
            $notification = array(
                'message' => 'There is something error' ,
                'alert-type' => 'error'
            );
        }
        return redirect()->back()->with($notification) ;
    }
}

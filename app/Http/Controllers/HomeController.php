<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        try {
            $user_id = Auth::id() ;
            $user_pass_status =DB::table('results')
                ->select('*')
                ->where('results.user_id',$user_id)
                ->get();
            if($user_pass_status->count() == 0)
            {
                $question = DB::table('questions')
                    ->select('*')
                    ->get();
                foreach($question as $q)
                {
                    $options[] = DB::table('options')
                        ->select('*')
                        ->where('options.question_id',$q->id)
                        ->get();
                }

                return view('home',compact('question','options'));
            }
            else
            {
                $data = $user_pass_status ;
                //dd($data[0]->total_points);
                return view('pass',compact('data'));
            }
        }
        catch(\Throwable $t)
        {
           return $t->getMessage();
        }


    }
    public function result(Request $request)
    {
        try{
            $user_id = Auth::id() ;
            $answers = $request->questions;
            $result = 0 ;
            $i = 0 ;
            foreach ($answers as $key => $value)
            {
                $points[] = DB::table('options')
                    ->select('points')
                    ->where('options.question_id',$key)
                    ->where('options.id',$value)
                    ->first();

            }
            $result = array_sum(array_column($points, 'points'));

            $result_id = DB::table('results')->insertGetId(
                [
                    'total_points' => $result,
                    'user_id' => $user_id,
                    "created_at" =>  \Carbon\Carbon::now(),
                    "updated_at" => \Carbon\Carbon::now(),
                ]
            );
            foreach ($answers as $key => $value) {
                DB::table('quest_resu')->insert(
                    [
                        'result_id' => $result_id,
                        'question_id' => $key,
                        'option_id' => $value,
                        'point' => $points[$i++]->points,
                        "created_at" =>  \Carbon\Carbon::now(),
                        "updated_at" => \Carbon\Carbon::now(),
                    ]
                );
            }
            return view('result',compact('result'));
        }catch(\Throwable $t)
        {
            return $t->getMessage() ;
        }


    }
}
